﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GironController : MonoBehaviour
{
  public GameObject[] gironTexts;

  public CameraController cameraController;

  // Update is called once per frame
  void Update()
  {

    StartCoroutine(cameraController.cameraRotationY(1f));

    Invoke("activeText1", 1f);
    Invoke("activeText2", 6.5f);
    Invoke("activeText3", 10.5f);
    Invoke("activeText4", 14f);
     Invoke("sceneBack", 18f);
  }

  public void activeText1()
  {
    gironTexts[0].SetActive(true);
  }
  public void activeText2()
  {
    gironTexts[1].SetActive(true);
  }
  public void activeText3()
  {
    gironTexts[2].SetActive(true);
  }
    public void activeText4()
  {
    gironTexts[3].SetActive(true);
  }

      public void sceneBack()
  {
      SceneManager.LoadScene("Scenes/Talk03");
  }
}

