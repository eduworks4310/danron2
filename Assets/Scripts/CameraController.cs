﻿
using UnityEngine;
using System.Collections;

public class CameraController : SingletonMonoBehaviourFast<CameraController>
{

  // 変数を定義する（データを入れる箱を作る）

  //public GameObject[] targets;
  public GameObject[] characters;
  public int targetInt;
  public float torqueFloat;

  public float followSpeed = 3.0f;
  public Camera[] cameras;

  public bool isCameraTargetFocus = false;

  public void cameraSwitching(int num)
  {
    for (int i = 0; i >= cameras.Length; i++)
    {
      if (i == num)
      {
        cameras[i].enabled = true;
      }
      else
      {
        cameras[i].enabled = false;
      }
    }
  }
  //ピンポイントフォーカスカメラ
  public IEnumerator cameraTargetFocus()
  {
    while(isCameraTargetFocus){
      Vector3 targetDir = characters[targetInt].transform.position - transform.position;
      float step = followSpeed * Time.deltaTime;
      Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 10.0F);
      transform.rotation = Quaternion.LookRotation(newDir);
      yield return new WaitForSeconds(0.05f);
    }
    isCameraTargetFocus= false;
  }


  public IEnumerator cameraTargetFocus(int index)
  {
    while(isCameraTargetFocus){
      Vector3 targetDir = characters[index].transform.position - transform.position;
      float step = followSpeed * Time.deltaTime;
      Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 10.0F);
      transform.rotation = Quaternion.LookRotation(newDir);
      yield return new WaitForSeconds(0.05f);
    }
    isCameraTargetFocus= false;
  }



  //回転カメラ横
  public IEnumerator cameraRotationY()
  {
    gameObject.transform.Rotate(0, 0.05f, 0);

    yield return new WaitForSeconds(.1f);

  }

  
  public IEnumerator cameraRotationY(float f)
  {
    
    gameObject.transform.Rotate(0, f, 0);

    yield return new WaitForSeconds(.1f);

  }

  //回転カメラZ
  IEnumerator cameraRotationZ(float speed)
  {

    this.transform.Rotate(0, 0, speed);
    yield return new WaitForSeconds(.1f);
  }

}

