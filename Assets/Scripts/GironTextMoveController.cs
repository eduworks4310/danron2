﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GironTextMoveController : MonoBehaviour
{
public Text gironText;
public float x=10;
public float y=10;
  void Start()
  {
  }

  // Update is called once per frame
  void Update()
  {
		GetComponent<RectTransform>().localPosition += new Vector3(x, y, 0)*Time.deltaTime;
  }
}
