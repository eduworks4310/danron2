﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandCameraForcus : ICommand, IPreCommand
{

  public string Tag
  {
    get { return "cam"; }
  }

  public void PreCommand(Dictionary<string, string> command)
  {
  }

  public void Command(Dictionary<string, string> command)
  {
    var cam = CameraController.Instance;
    var target = command["target"];
    cam.targetInt = int.Parse(target);
    cam.isCameraTargetFocus = true;
  }
}
