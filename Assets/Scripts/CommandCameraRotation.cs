﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandCameraRotation : ICommand, IPreCommand
{

  public string Tag
  {
    get { return "rotation"; }
  }

  public void PreCommand(Dictionary<string, string> command)
  {
  }

  public void Command(Dictionary<string, string> command)
  {
    var cam = CameraController.Instance;
    var torque = command["torque"];
    cam.torqueFloat = float.Parse(torque);
    cam.isCameraTargetFocus = true;
  }
}
