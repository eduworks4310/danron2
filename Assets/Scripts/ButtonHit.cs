﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonHit : MonoBehaviour
{
public GameObject tagObject;
  public void buttonHitTrue()
  {
   
    tagObject.SetActive(true);
         Invoke("sceneBack", 2f);
  }

  public void buttonHitFalse()
  {
    SceneManager.LoadScene("Scenes/Talk02");
  }

        public void sceneBack()
  {
      SceneManager.LoadScene("Scenes/Talk04");
  }
}